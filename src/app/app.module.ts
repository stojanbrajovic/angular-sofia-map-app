import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SofiaMapComponent } from './sofia-map/sofia-map.component';
import { MapComponent } from './map/map.component';
import { RegionsMenuComponent } from './regions-menu/regions-menu.component';


@NgModule({
  declarations: [
    AppComponent,
    SofiaMapComponent,
    MapComponent,
    RegionsMenuComponent
  ],
  imports: [
    BrowserModule,
    InlineSVGModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
