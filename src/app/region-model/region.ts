export interface Region {
  svgPathDescription: string;
  name: string;
}
