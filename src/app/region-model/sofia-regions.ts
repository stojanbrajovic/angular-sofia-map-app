import { Region } from './region';
import { svgText } from './sofiaMapSvgText';

const PATH_STRING = `<path d="`;
const pathDescriptions: string[] = [];
let indexOfPathString = svgText.indexOf(PATH_STRING);
while (indexOfPathString > -1) {
  const nextClosedQuoteIndex = svgText.indexOf(`"`, indexOfPathString + PATH_STRING.length);
  const regionSvgPathDescription = svgText.substring(indexOfPathString + PATH_STRING.length, nextClosedQuoteIndex);
  pathDescriptions.push(regionSvgPathDescription);
  indexOfPathString = svgText.indexOf(PATH_STRING, nextClosedQuoteIndex);
}

export const sofiaRegions_real: Region[] = [
  {
    name: 'zh.k. Lyulin 10',
    svgPathDescription: 'M280.3,309.5c-4.1,-3.1,-7.5,-6.2,-7.7,-6.7c-0.1,-0.5,2.9,-5.3,6.6,-10.7c5.8,-8.3,7.3,-9.9,10.2,-11c2.2,-0.8,4.6,-2.8,6.6,-5.2c1.7,-2.2,3.6,-3.9,4.1,-3.9c0.9,0,16.7,11.3,16.7,11.9c0,0.8,-11.6,17.9,-14.5,21.4c-3.5,4.2,-11.4,10.1,-13.3,10.1c-0.8,-0.1,-4.7,-2.7,-8.7,-5.9Z'
  },
  {
    name: 'zh.k. Lyulin 5',
    svgPathDescription: 'M182.2,313l-9.4,-7.2l-13.4,-0.4c-7.3,-0.3,-13.7,-0.7,-14.2,-1c-0.4,-0.2,2.3,-6.2,6.3,-13.5l7,-13l4,-0.5c5.7,-0.6,10.6,1.1,15.7,5.7c3.2,2.9,4.7,5,5.9,8.4c1.6,4.3,2.1,4.9,8.2,8.9c3.7,2.3,7.5,4.9,8.5,5.7l1.9,1.5L198,314c-2.6,3.5,-5.2,6.3,-5.6,6.3c-0.5,0,-5.1,-3.3,-10.2,-7.3Z'
  },
  {
    name: 'Levski G',
    svgPathDescription: 'M671.1,311.5c-1.9,-8,-3.4,-15,-3.4,-15.6c0,-1.4,0.8,-1.3,6.1,1.1c3.6,1.6,6,2,11,2c6.8,0,29.4,4.1,41.1,7.5l6.7,1.9l0,8.1l0,8.2l-3.5,-0.6c-1.9,-0.2,-7.1,-1,-11.7,-1.7c-12.9,-1.9,-20.1,-1.6,-30.9,1.2c-5.1,1.4,-9.9,2.5,-10.6,2.5c-1,0,-2.1,-3.4,-4.8,-14.6Z'
  },
  {
    name: 'zh.k. Lyulin 7',
    svgPathDescription: 'M245.5,314.2c-1,-7.1,-1.6,-13.2,-1.3,-13.6c0.2,-0.4,1.2,-0.8,2.2,-0.8c1,0,3.3,-1.1,5,-2.4c1.8,-1.4,3.8,-2.5,4.4,-2.5c1.4,0,29,21.1,29,22.1c0,0.8,-0.6,1.1,-8.6,3.9c-7.8,2.7,-17.3,5,-23.7,5.6l-5.2,0.6Z'
  },
  {
    name: 'zh.k. Lyulin 6',
    svgPathDescription: 'M202.2,326.3c-3.2,-0.8,-7,-2.9,-7,-3.9c0,-1.3,20.4,-28.3,21.4,-28.3c0.4,0,3.2,1.7,6.2,3.8l5.4,3.8l6.5,-0.4c6.2,-0.5,6.6,-0.4,6.9,1.3c0.7,3.3,2.9,20.6,2.9,22.4l0,1.9l-20.3,-0.1c-11.2,-0.1,-21.1,-0.3,-22,-0.5Z'
  },
  {
    name: 'Malashevtsi',
    svgPathDescription: 'M648,329.8c-5.4,-0.7,-8,-1.8,-18.9,-7.7l-12.7,-6.9l-6.4,0.5c-4.1,0.3,-6.5,0.2,-6.9,-0.4c-0.7,-1.1,14.3,-16.2,16.7,-16.8c1,-0.3,4.2,0.3,7,1.3c6.5,2.2,6.9,2.1,8.4,-0.7c1.1,-2,1.8,-2.3,6.4,-2.8c2.8,-0.3,8.7,-1.6,13,-2.9c4.3,-1.3,8.1,-2.1,8.5,-1.7c1,0.9,8.8,33.6,8.4,34.9c-0.4,0.9,-13.4,4.5,-15.9,4.3c-0.8,-0.1,-4.2,-0.6,-7.6,-1.1Z'
  },
  {
    name: 'kv. Gevgeliyski',
    svgPathDescription: 'M323,335.6c-6.3,-2.4,-11.5,-4.7,-11.7,-5.2c-0.2,-0.6,1.1,-1.7,2.9,-2.6c6,-3.1,19.2,-7.7,19.9,-6.9c0.9,0.8,4,18.1,3.4,18.7c-0.9,0.9,-3.3,0.2,-14.5,-4Z'
  },
  {
    name: 'zh.k. Banishora',
    svgPathDescription: 'M451.4,344.4c-0.4,-0.3,-0.6,-2.3,-0.6,-4.5c0,-3.8,-0.1,-3.9,-2.7,-4.2c-2.4,-0.2,-2.7,-0.5,-2.7,-2.7c0,-2.3,-0.3,-2.5,-4.4,-3.5c-5.3,-1.3,-6.7,-0.9,-6.7,2.1c0,5.7,-0.4,6,-6.8,5.8l-5.9,-0.3l-0.2,-3.4c-0.2,-2.7,-0.8,-3.8,-2.6,-5.2c-3.2,-2.2,-3.7,-2.1,-5.2,1.3c-1.2,2.3,-1.9,2.8,-3.8,2.8c-2.3,0,-2.8,-0.6,-6.7,-7.8c-2.3,-4.2,-4,-8.1,-3.7,-8.5c0.9,-1.5,20.1,-11.6,21.6,-11.4c3,0.4,7.6,-1.1,9.1,-3c0.9,-1.2,2,-2.1,2.6,-2.1c0.5,0,3,1.5,5.6,3.2c2.6,1.7,7.5,4.7,11,6.6c7,3.8,7.1,4,5.2,10.4c-0.7,2.2,-1.2,4.4,-1.2,5c-0.1,0.5,2,1.1,4.9,1.5l5,0.6l-0.5,3.4c-0.3,1.8,-1,5.7,-1.5,8.7l-1,5.3l-4.1,0.2c-2.3,0.2,-4.4,0,-4.7,-0.3Z',
  },
  {
    name: 'kv. Vrazhdebna',
    svgPathDescription: 'M740.5,350c-0.2,-1.7,-0.7,-5.5,-0.9,-8.6c-0.4,-4,-1,-5.8,-2,-6.6c-0.9,-0.5,-1.6,-2.3,-1.8,-3.8l-0.3,-2.9l7.3,0.2c5.2,0.1,7.7,0.5,8.7,1.4c1.9,1.8,5.5,1.6,8,-0.4c1.2,-0.9,2.8,-1.6,3.6,-1.6c2.1,0,2.6,-3.4,2.3,-15.5l-0.3,-10.4l3.2,-3.8l3.2,-3.9l11.8,0c8.9,0,12.6,-0.3,15.1,-1.3c2.5,-1,4.2,-1.2,7.1,-0.6c3.6,0.6,3.9,0.9,4.7,4c1.1,3.7,1.4,3.8,17.1,4.9c5,0.3,9.5,1.1,10.3,1.6c1.3,1,1.5,2.5,1.5,11.3c0,9.8,0,10.2,2.2,12.8c1.3,1.5,2.3,3,2.3,3.5c0,2.1,-3.6,7.3,-6.3,8.9c-2.9,1.8,-3.1,1.9,-6.7,0.5l-3.8,-1.4l-2.9,2.9c-2.8,2.7,-3.4,2.9,-11.9,3.9c-8.7,1,-9,1.2,-10.3,3.5c-1.4,2.5,-1.4,2.5,-11.2,3l-9.8,0.6L776.9,349c-2.1,-1.8,-4.3,-3.3,-4.8,-3.3c-0.5,0,-7.2,1.7,-14.7,3.7c-7.6,2.1,-14.4,3.7,-15.1,3.7c-0.9,0,-1.4,-1,-1.8,-3.1Z'
  },
  {
    name: 'Levski V',
    svgPathDescription: 'M714.5,352.9c-1.4,-2.7,-3.3,-4.5,-9.3,-8.5l-7.6,-5l-9,-0.6c-4.9,-0.3,-9.6,-0.7,-10.3,-0.9c-1.2,-0.4,-2.8,-6.1,-2.3,-8.3c0.1,-0.9,15.6,-5.2,22.1,-6.1c5.3,-0.8,14,0,29.3,2.7c5.3,1,6,1.7,6,6.2c0.1,1,0.8,2.5,1.7,3.3c1.2,1.1,1.8,3.3,2.4,8.8c0.5,4.1,0.9,7.7,0.9,8c0,0.3,-0.9,0.6,-2,0.6c-1.1,0,-5.2,0.7,-9.1,1.7c-10.2,2.3,-10.8,2.2,-12.8,-1.9Z',
  },
  {
    svgPathDescription: 'M394.1,357.3c-2,-0.6,-2.1,-0.9,-1.9,-6l0.2,-5.4l-3.1,-0.5c-1.7,-0.2,-5.1,-0.5,-7.5,-0.5c-4.4,0,-5,-0.2,-17.1,-8.4c-6.9,-4.6,-12.5,-8.7,-12.5,-9c0,-0.3,1.6,-3.2,3.7,-6.4c3.1,-4.9,3.6,-6,2.8,-7.4c-0.5,-1,-0.6,-2,-0.2,-2.4c1.5,-1.5,28.5,-10.6,29.3,-9.9c0.4,0.4,4.5,8.1,9.2,17l8.4,16.3l3.4,0.2c3.4,0.3,3.4,0.3,3.2,3.3c-0.3,3,-0.3,3,-3.5,2.7c-7,-0.7,-6.4,-1.3,-8.2,8.1c-1.8,9,-2.2,9.5,-6.2,8.3Z',
    name: 'Sveta Troitsa'
  },
  {
    svgPathDescription: 'M403.2,359.6c-2.1,-0.9,-2.1,-2.4,-0.1,-12c0.9,-4.1,1.1,-4.3,3.6,-4.3c4.7,0,5,0.6,3.4,7.9c-0.7,3.5,-1.5,7.1,-1.8,7.9c-0.4,1.5,-2.1,1.7,-5.1,0.5Z',
    name: 'Zona B-18'
  },
  {
    svgPathDescription: 'M571.4,361.2c-5.6,-2,-7.2,-2.8,-7.2,-4c0,-2.3,-4.5,-7,-9.9,-10.2c-4.7,-2.9,-5.7,-4.4,-5.7,-8.8c0,-1.8,1.2,-3.1,7.2,-7.4c8.6,-6.4,12.3,-7.7,21.9,-8.4c4.1,-0.3,10,-1.1,13,-1.7c3.1,-0.7,10,-1.6,15.4,-2.1l9.8,-0.9l9.4,5c5.2,2.8,10.2,5.7,11.2,6.5l1.8,1.5l-4.1,3.4c-9.1,7.6,-18.3,13.2,-26.3,15.8c-4.7,1.6,-11.8,4.9,-17.5,8.2c-5.3,3.1,-10.1,5.6,-10.7,5.6c-0.6,0,-4.4,-1.2,-8.3,-2.5Z',
    name: 'Hadzhi Dimitar'
  },
  {
    svgPathDescription: 'M362.4,364.4c-9.4,-1.8,-17.5,-3.5,-17.8,-3.8c-0.7,-0.7,-7.4,-38.5,-7.1,-40.5c0.2,-1.2,2.2,-2.3,7.7,-4.4c11.9,-4.5,13.5,-3.1,7.3,6.2c-3.4,5.1,-3.7,6,-2.7,7c0.6,0.7,6.9,5.1,13.9,9.8l12.7,8.7l5.5,0c3,0,6,0.2,6.7,0.4c1.5,0.6,1.1,4.6,-1.3,13.7l-1.6,6.4l-3,-0.1c-1.7,-0.1,-10.8,-1.6,-20.3,-3.4Z',
    name: 'Ilinden'
  },
  {
    svgPathDescription: 'M309,371.8c-0.2,-1,-0.4,-2.6,-0.4,-3.5c0.1,-5.2,-0.2,-10.2,-1.2,-19.7c-1.6,-14.3,-1.6,-16,0.2,-15.9c0.8,0,8.1,2.5,16.2,5.5l14.8,5.5l1.8,10.3c1.1,5.6,1.7,10.6,1.5,11.2c-0.2,0.6,-1.1,1,-2.1,1c-1,0,-2.3,1.1,-3.3,2.8c-1.9,3.2,-2.2,3.2,-18.6,4.2c-8.3,0.5,-8.4,0.5,-8.9,-1.4Z',
    name: 'Zapaden park'
  },
  {
    svgPathDescription: 'M679.6,389.2c-0.5,-3,-0.8,-3.3,-2.9,-3.3c-1.3,0,-2.6,-0.4,-2.8,-0.9c-0.3,-0.5,-0.1,-4,0.4,-7.8c1.3,-9,1.1,-9.4,-3.7,-8.4c-2.1,0.5,-4,0.7,-4.2,0.5c-0.3,-0.3,-0.8,-3.1,-1.3,-6.4c-1.1,-8.2,-3.5,-14,-7.7,-19.3c-2.8,-3.3,-3.7,-5.1,-3.7,-7.3c0,-2.4,0.4,-2.9,1.8,-2.9c0.9,0,4.6,-0.7,8.3,-1.6c3.6,-1,7.1,-1.6,7.8,-1.5c0.8,0.2,1.8,2.1,2.6,5.2l1.3,4.9l10.7,0.7l10.6,0.7l7.1,4.8c5.5,3.7,7.4,5.4,8.6,8.1c2,4.4,3.9,4.8,12.7,2.9c3.9,-0.8,7.9,-1.6,8.9,-1.8c4.5,-1,-0.2,5.5,-10.4,14.6c-9.3,8.2,-14.7,11.2,-26.7,15.2c-7.7,2.5,-9.5,3.4,-9.7,4.8c-0.2,1.3,-1,1.7,-3.7,1.9c-3.4,0.2,-3.4,0.2,-4,-3.1Z',
    name: 'kv. Levski'
  },
  {
    svgPathDescription: 'M332.4,391.2c-10.3,-1.2,-20.1,-4.4,-20.8,-6.8c-0.2,-0.8,-0.6,-2.9,-0.9,-4.8l-0.6,-3.4l8.5,-0.5c17.5,-1.1,17.6,-1.1,19.1,-4.2c0.9,-2.1,1.8,-2.8,3.3,-2.8c2.3,0,1.9,-1.1,4.5,14.1c0.8,4.8,1.3,9,1.1,9.2c-0.6,0.6,-2.9,0.4,-14.2,-0.8Z',
    name: 'Krasna polyana 1'
  },
  {
    svgPathDescription: 'M380.1,395.7c0,-0.5,1.6,-6,3.4,-12.3c4,-13.2,3,-12.5,14.3,-9.7c7.7,1.9,7.9,2.2,5.4,7.2c-1.3,2.5,-3.8,4.5,-11.4,9.4c-9.6,6.1,-11.7,7.1,-11.7,5.4Z',
    name: 'Zona B-19'
  },
  {
    svgPathDescription: 'M430.2,395.7c0,-0.9,0.5,-4.2,1,-7.4l1,-5.9l3.4,0.5c4.8,0.7,9.4,2.2,9.4,3c0,0.9,-12.7,11.5,-13.9,11.5c-0.5,0,-0.9,-0.8,-0.9,-1.7Z',
    name: 'Zona B-5-3'
  },
  {
    svgPathDescription: 'M553.2,405.8c-4,-1.2,-4,-1.2,-3.5,-4.2c0.3,-1.7,0.5,-5.5,0.5,-8.5l0,-5.6l8.2,0c9.6,0,9.1,-0.6,7.1,9.9c-1,4.9,-1.6,6.4,-3.5,7.9c-2.7,2.2,-3.3,2.2,-8.8,0.5Z',
    name: 'Oborishte'
  },
  {
    svgPathDescription: 'M414.3,406.6c-0.6,-1.3,-1.4,-4,-1.7,-6.1c-0.4,-3.3,-0.9,-3.9,-3,-4.7c-2.6,-0.9,-3.5,-2.6,-4.4,-8.5c-0.7,-4.9,1.9,-11.6,4.3,-10.9c0.8,0.2,5.5,1.4,10.5,2.6c4.9,1.2,9.2,2.4,9.5,2.7c0.5,0.5,-2.8,20.3,-3.5,20.9c-3.4,3.2,-7.8,6.2,-9,6.2c-0.8,0,-2,-1,-2.7,-2.2Z',
    name: 'Zona B-5'
  },
  {
    svgPathDescription: 'M531.2,408c-2.5,-1.3,-3.5,-1.4,-5.6,-0.7c-1.4,0.5,-2.7,0.8,-2.8,0.6c-0.1,-0.2,-0.8,-1,-1.5,-1.9c-1.3,-1.6,-1.3,-2.2,0.5,-9.9l1.8,-8.2l11.7,-0.2L547,387.5l-0.3,8.8l-0.2,8.8l-5.7,2.3c-3.2,1.2,-6,2.2,-6.2,2.2c-0.2,0,-1.8,-0.7,-3.4,-1.6Z',
    name: 'Doktorski pametnik'
  },
  {
    svgPathDescription: 'M109.8,408.1l-9.5,-4.7l-0.2,-4.3c-0.2,-3.1,-0.7,-4.5,-1.9,-5.2c-0.9,-0.6,-1.6,-1.6,-1.6,-2.3c0,-2.8,-2.9,-4.2,-9.8,-4.7c-5.9,-0.4,-6.9,-0.7,-8.3,-2.5c-2.2,-2.7,-2,-4.2,0.3,-4.2c1.4,0,3.4,-1.7,7.1,-6.2c2.9,-3.4,5.6,-6.1,6,-5.9c0.5,0.1,1.6,1.6,2.5,3.2c1.7,2.9,1.7,3.1,0.3,6c-2.3,4.8,-1.1,5.1,8.7,2.4c8.4,-2.3,9.9,-3.2,13.6,-8.1l1.8,-2.5l1.6,1.9c0.9,1,4.9,3.3,8.8,5l7.3,3.1l0.2,15.3l0.2,15.2l-2.3,0.5c-1.2,0.3,-4.3,1.1,-6.9,1.7c-2.5,0.6,-5.5,1.1,-6.6,1.1c-1,0,-6.1,-2.2,-11.3,-4.8Z',
    name: 'kv. Suhodol'
  },
  {
    svgPathDescription: 'M313.1,412.1c-1.3,-1.4,-1.4,-2.7,-0.9,-11.5c0.2,-5.4,0.7,-10.2,0.9,-10.6c0.3,-0.5,1.7,-0.3,3.6,0.5c1.6,0.7,6.7,1.7,11.2,2.2c4.4,0.6,8.2,1.4,8.4,1.8c0.7,2.2,-17.9,19.3,-20.9,19.3c-0.4,0,-1.4,-0.8,-2.3,-1.7Z',
    name: 'Krasna polyana 2'
  },
  {
    svgPathDescription: 'M344,414.5l-4.4,-5l3.6,-3c2,-1.6,4.5,-3.8,5.6,-4.7l2.1,-1.8l-3.1,-17.5c-1.6,-9.6,-2.7,-17.8,-2.3,-18.1c0.7,-0.8,37,5.7,38.2,6.8c1.2,1.1,-6.5,27.7,-9.2,31.7c-2,3.1,-22.8,16.6,-25.4,16.6c-0.3,0,-2.6,-2.3,-5.1,-5Z',
    name: 'zh.k. Razsadnika'
  },
  {
    svgPathDescription: 'M353.8,423.2l-1.9,-2l2.7,-1.5c7.4,-4,19.7,-12.6,22.3,-15.6c3,-3.5,24.2,-17.7,25.4,-17c0.4,0.3,1,2.1,1.3,4c0.6,4.4,0.9,4.9,4.2,6.6c2.1,1.1,2.7,1.9,2.7,3.8c0,1.3,0.6,3.7,1.4,5.3c0.8,1.5,1.2,3.1,0.9,3.3c-0.3,0.3,-6.6,2.8,-13.9,5.7c-11.8,4.5,-14.4,5.2,-21.3,5.7c-4.3,0.3,-10.1,1.3,-13,2.1c-6.8,2.1,-8.5,2,-10.8,-0.4Z',
    name: 'Serdika'
  },
  {
    svgPathDescription: 'M332.3,419.3c-2.9,-3.3,-5.7,-6.6,-6.3,-7.4c-1,-1.2,-0.2,-2.3,6.9,-9.5c7.7,-7.9,8.1,-8.2,11,-7.7c6,0.9,5.6,3.6,-1.6,9.1c-3.2,2.5,-5.7,4.8,-5.7,5.3c0,0.5,2.2,3.1,4.9,6c2.7,2.8,4.9,5.4,4.9,5.9c0,1.2,-5.2,4.2,-7.2,4.2c-1,0,-3.6,-2.2,-6.9,-5.9Z',
    name: 'Krasna polyana 3'
  },
  {
    svgPathDescription: 'M626.6,436.7c-1.5,-0.3,-5.6,-1.6,-9,-2.8c-3.4,-1.1,-9.7,-3,-14,-4.2c-4.3,-1.2,-8.2,-2.5,-8.7,-3.1c-0.8,-0.7,-0.5,-2.2,1.4,-6.2c2,-4.4,2.5,-6.9,3.2,-16.3c0.5,-6.1,1,-11.3,1.1,-11.4c0.2,-0.1,3.1,-0.5,6.5,-0.8l6.2,-0.5l10.6,5.5c5.8,3,11.2,6.2,12,7c1.5,1.4,1.5,1.6,-0.6,5.7c-1.6,3.1,-2.1,5.3,-1.9,7.7c0.2,3.2,0.4,3.4,2.9,3.7c2.2,0.2,3,0.9,5.2,4.7c1.4,2.5,2.4,4.7,2.3,4.9c-0.2,0.1,-2.1,1,-4.3,2c-2.2,0.9,-4.9,2.4,-6.1,3.3c-2.2,1.7,-2.4,1.8,-6.8,0.8Z',
    name: 'Reduta'
  },
  {
    svgPathDescription: 'M289.4,428.5l-9.3,-9l-4.3,0.6l-4.4,0.5l0.5,-2.9c0.4,-2.8,0.2,-3.1,-4.2,-6.3c-2.5,-1.8,-4.8,-3.1,-5.2,-2.9c-0.4,0.2,-0.7,1.5,-0.7,2.8c0,2.1,-0.4,2.5,-2,2.5c-1.6,0,-2.2,-0.7,-2.9,-2.9l-1,-2.8l-4.4,1.6c-2.4,0.8,-8.6,1.9,-13.8,2.5c-9.1,0.9,-13.2,0.7,-11.6,-0.8c1.5,-1.3,11.4,-4.2,14.5,-4.2l3.2,0l-0.3,-3.5c-0.2,-3.2,-0.4,-3.5,-2.8,-3.7c-2.4,-0.2,-2.7,-0.6,-2.9,-3.3l-0.3,-3l-8.1,-0.6c-7.8,-0.6,-8.3,-0.7,-10.9,-3.3c-2.6,-2.6,-2.6,-2.8,-1.8,-6.6c0.5,-2.1,1.1,-3.9,1.5,-3.9c1.4,0,6.3,2.4,8.5,4.2l2.3,1.8l2.1,-2c1.6,-1.5,2,-2.5,1.6,-4.6c-0.3,-2.2,0,-2.9,1.9,-4.2c2,-1.4,2.5,-1.5,3.9,-0.5c0.9,0.6,2.1,2.6,2.7,4.5c1,3.1,1.3,3.3,4.2,3.3c2.1,0,3.9,-0.7,6.1,-2.5c1.7,-1.3,3.3,-2.4,3.5,-2.4c0.2,0,2.5,1.9,5,4.2c4.5,4.1,4.6,4.1,9.1,3.8c2.5,-0.2,5.4,0,6.4,0.4c3.5,1.6,7.8,-0.5,12.8,-6.2c3.7,-4.2,4.4,-5.4,3.7,-6.6c-0.4,-0.8,-0.6,-1.7,-0.4,-1.9c0.8,-0.8,13.7,-3.4,14.1,-2.8c0.2,0.3,1.2,5.2,2.2,10.8c1.3,7.5,1.7,13,1.6,21.2c-0.1,9.9,0.1,11.4,1.6,13.5c0.9,1.4,1.6,2.9,1.6,3.5c0,1.7,-11.8,20.7,-12.9,20.7c-0.6,0,-5.2,-4,-10.4,-9Z',
    name: 'kv. Fakulteta'
  },
  {
    svgPathDescription: 'M570.4,428.7c-7.8,-6.6,-12.7,-11.3,-13.2,-12.8c-0.7,-2.1,-0.5,-2.5,2.4,-4.9c7.6,-6,7.1,-4.9,9.4,-18.1c2.1,-11.9,2.2,-12.2,4.5,-13c3.5,-1.1,17.4,4.3,21.4,8.4l3,3l-0.8,11.8l-0.9,11.8L590.7,427c-3.7,8.2,-5.9,12.1,-6.8,12.1c-0.7,0,-6.8,-4.7,-13.5,-10.4Z',
    name: 'Poduyane'
  },
  {
    svgPathDescription: 'M662.8,426C654,417.8,644.2,408.6,641,405.6c-4.5,-4.3,-8,-6.7,-16.4,-11l-10.7,-5.4l-7.3,0c-7.2,0,-7.3,0,-10.8,-3.2c-2,-1.7,-6.2,-4.1,-9.6,-5.4c-3.3,-1.3,-6.3,-2.6,-6.7,-2.9c-0.3,-0.3,0.1,-3.2,1.1,-6.4l1.8,-5.7l9.3,-5.5c5.5,-3.2,12.7,-6.5,17.5,-8.1c8.2,-2.8,10.7,-4.3,24.5,-14.6c7.6,-5.8,9.1,-6.3,15,-5.1c2.5,0.5,2.6,0.7,2.6,4.4c0,2.8,0.4,4.3,1.6,5.3c5.7,5.2,9.3,13.6,10.4,24.4l0.5,5.7l4.2,-0.3l4.2,-0.3l-0.3,4.6c-0.2,2.4,-0.4,6.1,-0.4,8.2c-0.1,3.6,0,3.7,2.8,4.1c2.2,0.3,2.9,0.7,2.9,2c0,1.4,-0.7,1.7,-5.2,1.9c-3.5,0.1,-5.3,0.6,-5.6,1.4c-0.3,0.6,3.8,9.5,9.1,19.6c10.5,20.6,11.2,22.9,7.1,25.8c-1.3,0.9,-2.7,1.7,-3.1,1.7c-0.4,0,-7.9,-6.7,-16.7,-14.8Z',
    name: 'zh.k. Suhata reka'
  },
  {
    svgPathDescription: 'M398.4,443.9c-1,-2.4,-1.9,-4.6,-1.9,-4.9c0,-0.8,19.6,-13.9,21.4,-14.3c1.7,-0.5,12.7,10.6,12.1,12.2c-0.4,1.1,-26.2,11.3,-28.6,11.3c-0.6,0,-2,-2,-3,-4.3Z',
    name: 'Meditsinska akademiya'
  },
  {
    svgPathDescription: 'M243,451.8c-2.7,-1.7,-4,-2,-5.3,-1.4c-1.1,0.5,-3.1,0.5,-5.1,0.1c-2.6,-0.6,-4.3,-0.4,-7.1,0.6c-1.9,0.7,-4.7,1.5,-6.2,1.8c-2.6,0.4,-2.6,0.3,-3.5,-4.4c-0.6,-2.7,-2.4,-11.6,-4.2,-20c-1.7,-8.3,-2.9,-15.4,-2.7,-15.8c0.2,-0.3,2.9,-0.6,6,-0.6c4.6,0,5.8,0.3,7.1,1.8c1.5,1.6,2.1,1.7,9.4,1.2c4.3,-0.2,9,-0.7,10.4,-1c2.6,-0.5,2.7,-0.5,6.1,6.3c2.9,5.9,4.1,7.5,8.7,11c3,2.2,5.8,4.3,6.3,4.6c0.5,0.4,0,1.8,-1.4,3.9c-1.3,2,-2.2,4.5,-2.2,6.2c0,2.4,-0.3,2.9,-1.7,2.9c-0.9,0,-3.3,1.1,-5.3,2.5c-1.9,1.3,-4,2.4,-4.6,2.4c-0.6,0,-2.7,-1,-4.7,-2.1Z',
    name: 'kv. Ovcha kupel 1'
  },
  {
    svgPathDescription: 'M194.7,466.1c-1.7,-1.4,-10.5,-7.5,-19.4,-13.6C160.2,442.1,159.1,441.2,159.6,439.2c0.6,-3.1,10.3,-22.9,11.7,-24.1c2.1,-1.7,10.8,-4.5,15.9,-5.1c2.7,-0.3,7.6,-0.1,11.4,0.5c5.4,0.8,6.7,1.3,7.3,2.8c0.3,0.9,2.4,10.3,4.5,20.8l3.9,19.1l-3.1,3.2c-2.1,2.2,-3.9,3.3,-5.4,3.3c-1.9,0,-2.4,0.6,-4,4.5c-1,2.4,-2.3,4.5,-2.9,4.5c-0.5,0,-2.4,-1.2,-4.2,-2.6Z',
    name: 'kv. Ovcha kupel 2'
  },
  {
    svgPathDescription: 'M700.8,458.7c-10.1,-8.2,-18.3,-15.3,-18.3,-15.8c0,-0.5,1.3,-1.7,2.8,-2.7c2.6,-1.6,2.8,-2.1,2.5,-4.9c-0.2,-1.7,-4.5,-11.2,-9.4,-21.1c-4.9,-9.9,-8.8,-18.2,-8.5,-18.4c0.2,-0.3,4.7,-0.7,10,-0.9c9.5,-0.4,9.6,-0.4,9.8,-2.5c0.3,-1.8,1.1,-2.4,8.2,-4.7c4.3,-1.4,8.2,-2.4,8.6,-2.1c0.4,0.2,0.7,4,0.7,8.4l0,8l7.6,5.5c6.4,4.7,9.1,7.5,17.3,17.9l9.8,12.4l-1.8,8.2c-1,4.6,-2.8,11.1,-3.9,14.5l-2.2,6.2l-6.4,3.4c-3.5,1.9,-6.8,3.5,-7.5,3.5c-0.6,0,-9.3,-6.7,-19.3,-14.9Z',
    name: 'Hristo Botev'
  },
  {
    svgPathDescription: 'M404,472.9c-1,-2,-2.7,-20.8,-1.9,-21.6c0.3,-0.3,7.7,-3.2,16.2,-6.4c8.6,-3.2,15.9,-6.1,16.2,-6.4c0.4,-0.4,-3.2,-4.4,-7.8,-9.1L418.2,421l-11,7.5c-6,4,-11.4,7.4,-11.9,7.4c-1.2,0,-7.2,-12.4,-6.4,-13.2c0.4,-0.4,7.8,-3.3,16.5,-6.6l15.8,-6l12.7,-11c10.7,-9.3,12.8,-11.4,12.8,-13.3c0,-1.3,-0.4,-2.5,-0.8,-2.7C445.5,382.8,432.5,379.6,417,376c-15.5,-3.6,-28.4,-6.7,-28.6,-6.9c-0.5,-0.6,1.6,-9.3,2.4,-9.9c0.4,-0.2,4.7,0.7,9.5,2.1c4.8,1.4,9.1,2.3,9.4,1.9c1.1,-1.2,4.9,-21.1,4.9,-25.6c0,-8,3.6,-10,4.3,-2.5c0.2,2.3,0.6,4.3,0.8,4.5c0.2,0.2,3.6,0.4,7.6,0.6c8.2,0.2,7.7,0.5,9.1,-6.2c0.4,-2,0.8,-2.3,3.1,-2c2.4,0.2,2.7,0.5,2.9,3.2c0.2,2.8,0.5,3.1,2.9,3.3c2.5,0.3,2.6,0.5,3,4.4l0.4,4.1l6.2,0.5c3.4,0.3,6.5,0.2,6.8,-0.1c0.9,-0.8,4.1,-20.4,3.5,-21.9c-0.2,-0.7,-1.7,-1.1,-4,-1.1c-4.9,0,-5.6,-0.9,-4.1,-5.4c0.7,-2,1.5,-3.6,1.9,-3.6c0.4,0,4.1,1.5,8.3,3.3c6.6,2.9,7.9,3.2,10,2.5c2.2,-0.8,3.9,-0.4,12.8,3c8.4,3.1,11.3,4.7,16.5,8.8c6,4.9,6.3,5.2,8.7,11.9l2.5,7L535,363.5c11.7,7.9,19.9,12.7,25.3,15.1c5.9,2.5,8,3.7,7.6,4.6c-0.2,0.7,-0.4,1.3,-0.4,1.5c0,0.2,-10.3,0.1,-22.9,-0.2L521.7,384l-2.3,9.8c-1.2,5.3,-2.4,9.9,-2.5,10.2c-0.2,0.3,1,2,2.6,3.9l2.9,3.4l2.9,-1.3c2.8,-1.1,3.2,-1.1,5.8,0.5c3.6,2.1,3.8,2.1,10.7,-0.8l5.8,-2.4l4.8,1.5l4.8,1.4l-2.5,1.3c-1.3,0.7,-6.8,2.6,-12.1,4.3c-10.2,3.1,-15.1,6.2,-21.3,13.4c-2.7,3.2,-11,8.9,-17.8,12.4c-1.9,0.9,-9.1,3.9,-16,6.5c-6.9,2.6,-15.1,6.2,-18.2,8c-12.9,7.1,-15.1,7.6,-28.5,7.6l-12.1,0l-10.1,4.6c-14.2,6.4,-13.7,6.3,-14.6,4.6Z',
    name: 'Tsentar'
  },
  {
    svgPathDescription: 'M332.3,471.1c-3.7,-3.2,-7,-6.1,-7.1,-6.5c-0.1,-0.5,1.1,-2,2.9,-3.5c9.3,-7.9,19.5,-18.6,21.6,-22.8c4.9,-9.5,12.4,-13.2,28.6,-14.2l7.6,-0.5l3.3,6.6c1.8,3.7,3.2,7.2,3.1,7.8c-0.3,1.1,-51.3,38.8,-52.6,38.8c-0.3,0.1,-3.6,-2.5,-7.4,-5.7Z',
    name: 'Lagera'
  },
  {
    svgPathDescription: 'M581.2,467.2c-7,-6.6,-18.4,-18.1,-25.4,-25.6c-6.9,-7.4,-14.3,-14.9,-16.5,-16.7c-2.4,-1.8,-3.7,-3.4,-3.3,-3.8c0.6,-0.7,16.2,-5.7,17.7,-5.7c0.4,0,1.3,1.3,2,2.8c0.9,1.8,5.7,6.4,13.5,12.8c6.7,5.6,12.2,10.4,12.2,10.9c0,0.4,-0.5,1.4,-1.2,2.2c-1.1,1.2,-0.6,1.9,5.8,7c4.8,3.8,8.5,7.7,11.5,12c2.5,3.5,4.5,6.6,4.5,6.9c0,1,-6.6,9.3,-7.4,9.3c-0.4,0,-6.4,-5.4,-13.4,-12.1Z',
    name: 'Yavorov'
  },
  {
    svgPathDescription: 'M342.1,479.8c-0.3,-0.7,7.8,-7.1,24.7,-19.4c13.9,-10,25.8,-18.4,26.5,-18.6c1,-0.2,1.8,0.9,3.1,4c1.8,4.3,2.7,8.8,4.3,21.6c1,8.2,1,8.2,-4.9,8.9c-2.1,0.2,-5.8,1.4,-8.3,2.5c-5.9,2.8,-7.4,2.7,-16.3,-0.7c-12.2,-4.7,-15.8,-4.6,-23.8,0.8c-3.4,2.3,-4.7,2.6,-5.3,0.9Z',
    name: 'zh.k. Hipodruma'
  },
  {
    svgPathDescription: 'M671,497.6c-10.7,-3.9,-33.7,-14.9,-33.7,-16.1c0,-0.9,6.2,-14.3,6.9,-15.1c0.8,-0.8,1.8,-0.7,5.2,0.7l4.1,1.8l3,-2.4c2.7,-2.1,3,-2.7,3,-6.5c0,-7,-9,-30.7,-11.6,-30.7c-0.8,0,-2.8,-2.1,-4.7,-5.2c-2.3,-3.6,-4,-5.4,-5.2,-5.6c-1.3,-0.1,-2,-0.9,-2.2,-2.2C635.5,414,638,408,639.3,408c0.5,0,9,7.6,19,16.9c9.9,9.3,21,19.2,24.6,22.2l6.6,5.4l0.5,6.9c0.9,10.9,-1.3,17.9,-9.5,29.5c-3.6,5.1,-6.9,9.2,-7.4,9.2c-0.5,0,-1.4,-0.3,-2.1,-0.5Z',
    name: 'zh.k. Slatina'
  },
  {
    svgPathDescription: 'M303.8,495.8c-1.4,-2.1,-2.6,-4.2,-2.6,-4.6c0,-0.3,3.6,-4.4,7.9,-9.1l7.9,-8.4l-6.7,-4.4c-3.6,-2.4,-6.6,-4.7,-6.6,-5.2c0,-0.8,4.6,-4.4,8,-6.4c0.8,-0.5,23.3,17.7,25,20.3c0.4,0.5,0.2,1.3,-0.4,1.8c-1.6,1.6,-28.7,20,-29.4,20c-0.3,0,-1.7,-1.8,-3.1,-4Z',
    name: 'Slaviya'
  },
  {
    svgPathDescription: 'M410,497.6c-0.3,-1.7,-1.2,-6.6,-2.1,-10.9c-0.9,-4.3,-1.7,-8.3,-1.8,-9c-0.1,-0.8,3.7,-2.9,11,-6.3l11.1,-5.1l9,0c5,-0.1,9.6,0.3,10.2,0.7c0.9,0.5,-0.6,3.7,-7.5,15.3l-8.7,14.6l-5.8,-0.2c-5.1,-0.1,-6.3,0.1,-9.1,1.9c-4.5,2.8,-5.6,2.6,-6.3,-1Z',
    name: 'Ivan Vazov'
  },
  {
    svgPathDescription: 'M359.6,497.4C344.2,482.6,344.4,483.1,352.5,479c4.5,-2.3,4.7,-2.4,9.3,-1.4c2.6,0.6,7.5,2.2,10.9,3.5c5.2,2.1,6.8,2.4,10.5,1.9c2.9,-0.3,4.3,-0.2,4.3,0.4c0,1.4,-13.1,27.1,-13.8,27c-0.3,0,-6.7,-5.9,-14.1,-13Z',
    name: 'Belite Brezi'
  },
  {
    svgPathDescription: 'M169.9,510.3c-4.8,-6.8,-9.1,-10.3,-14.5,-11.5c-4.2,-0.9,-5.6,-0.9,-8.9,0c-4.7,1.3,-6.4,0.9,-5.8,-1.3c0.3,-1.1,-0.1,-2,-1.3,-2.6c-1,-0.5,-1.7,-1.7,-1.7,-2.9c0,-1.6,-0.4,-2,-2,-2c-1,0,-2.3,-0.5,-2.7,-1.1c-0.9,-1.1,-0.2,-15.9,0.8,-17.5c0.4,-0.5,0.2,-1.9,-0.3,-3c-1.3,-2.8,0.8,-6.3,3.8,-6.3c2.1,0,5,-3.4,4.1,-4.8c-0.8,-1.5,3.1,-6.6,5.8,-7.5c1.4,-0.5,4,-1.9,5.8,-3.3c1.7,-1.3,3.7,-2.4,4.3,-2.4c1.6,0,35.1,23.2,42.2,29.3c6.2,5.3,6.3,5.5,6.8,10c0.3,2.5,1.4,8,2.5,12.2c1.1,4.3,1.7,8.1,1.5,8.5c-0.3,0.5,-3.6,0.6,-8.2,0.2l-7.7,-0.5l-6.8,3.9c-3.7,2.2,-7.8,4.2,-9.2,4.5c-1.4,0.3,-3.3,0.8,-4.3,1.1c-1.4,0.5,-2.2,-0.1,-4.2,-3Z',
    name: 'Gorna Banya'
  },
  {
    svgPathDescription: 'M313.6,525.3l-2.9,-2.4l4,-13.8l3.9,-13.8l9.6,-6.6c5.3,-3.7,10.1,-6.9,10.6,-7.3c1.2,-0.7,11.7,9,11.7,10.8c0,2.6,-8,12.2,-14.9,17.9c-4,3.2,-9.5,8.6,-12.3,11.8c-2.8,3.2,-5.5,5.8,-6,5.7c-0.5,0,-2.2,-1,-3.7,-2.3Z',
    name: 'Krasno Selo'
  },
  {
    svgPathDescription: 'M635.2,518.6c-5.4,-5.4,-16.2,-15.8,-24,-23.1C603.4,488.2,597,482,597,481.7c0,-0.3,1.9,-3,4.2,-5.9l4.2,-5.5l-4.8,-7.1c-3.2,-4.7,-6.9,-8.9,-11.1,-12.5l-6.4,-5.4l2,-2c1.1,-1.2,3.1,-4.8,4.6,-8c1.4,-3.3,3,-6,3.5,-6c1.9,0,12.1,3.7,12.1,4.4c0,0.4,-1.4,2.3,-3,4.3c-1.6,1.9,-2.7,3.9,-2.4,4.4c0.3,0.4,2,1.3,3.8,1.8c3,0.9,3.5,0.8,5,-0.7c1.3,-1.4,2.2,-1.6,4.3,-1c1.4,0.3,3.5,0.8,4.7,1.1c1.7,0.4,2.3,1.1,2.5,2.7c0.2,2,0.7,2.3,3.1,2.5c2,0.2,3.7,1.1,5.5,3c1.4,1.5,4.8,3.8,7.5,5.1c2.8,1.3,5.4,3.1,5.8,4c0.6,1.2,-0.2,3.9,-3.3,10.9c-2.2,5.1,-3.9,9.8,-3.9,10.3c0,2,22,12.9,34.7,17.2c0.8,0.2,1.4,1,1.4,1.8c0,0.7,-5.4,6.6,-11.9,13c-6.6,6.5,-11.9,12.3,-11.9,13.1c0,2.7,-2.4,1,-12,-8.6Z',
    name: 'kv. Geo Milev'
  },
  {
    svgPathDescription: 'M288.9,535c-6.1,-3.5,-11.2,-6.9,-11.3,-7.5c-0.2,-0.7,3.3,-4,7.8,-7.5c4.4,-3.4,12.3,-9.6,17.4,-13.6c5.2,-4.1,9.7,-7.4,10.1,-7.4c2.2,0,1.1,4.6,-6.8,29.7c-2.4,7.6,-4.5,12.9,-5.1,12.8c-0.6,0,-6,-2.9,-12.1,-6.5Z',
    name: 'zh.k. Bakston'
  },
  {
    svgPathDescription: 'M553.5,541c0,-0.8,0.9,-6.4,2.1,-12.5c1.1,-6.1,2,-11.3,2,-11.6c0,-0.4,2.4,-2.3,5.3,-4.4l5.4,-3.8l4.3,2.3c7.2,3.9,9.7,7.1,12.9,16.1c1.5,4.6,2.4,8.3,2,8.7c-1.2,1.1,-9.1,2.5,-14.3,2.5c-2.9,0,-7.4,0.8,-11.2,2.1c-7.7,2.4,-8.5,2.5,-8.5,0.6Z',
    name: 'kv. Izgrev'
  },
  {
    svgPathDescription: 'M399.7,538.6c-1.5,-2.2,-7.5,-8.9,-13.4,-14.8L375.6,512.9l8.2,-16.5c4.5,-9,8.8,-16.7,9.5,-17.1c0.6,-0.3,3.1,-0.9,5.5,-1.2c4,-0.5,4.3,-0.4,5.1,1.4c1.3,3.6,5,24.6,5,28.8c-0.1,5.3,-4.6,32.5,-5.7,33.5c-0.5,0.6,-1.8,-0.6,-3.5,-3.2Z',
    name: 'Strelbishte'
  },
  {
    svgPathDescription: 'M334.6,536.8c-7.7,-3.5,-14.1,-6.7,-14.2,-7.1C320,528.4,330.6,517,337.5,511.4c3.5,-2.9,8.3,-7.8,10.6,-10.9c2.3,-3.1,4.7,-5.6,5.2,-5.6c1.1,0,18.6,17.3,18.6,18.3c0,1.3,-21.7,30,-22.6,30c-0.4,0,-7.1,-2.9,-14.7,-6.4Z',
    name: 'Borovo'
  },
  {
    svgPathDescription: 'M230.4,525.8c-8.3,-10,-15.4,-18.7,-15.8,-19.2c-1.3,-2.2,-4.7,-15.7,-5.5,-22c-0.7,-6.2,-0.9,-6.6,-4.3,-9.5l-3.4,-3l1.7,-4.8c1.4,-4,2,-4.8,3.8,-5c1.1,-0.2,3.5,-1.7,5.2,-3.5c2.6,-2.7,3.7,-3.2,6.3,-3.2c1.8,0,4.4,-0.6,5.8,-1.4c1.9,-0.9,3.9,-1.2,7.6,-0.9c2.8,0.2,5.6,0.1,6.3,-0.3c0.7,-0.4,2.5,0.2,5,1.8c2.1,1.3,4.3,2.4,4.7,2.4c0.5,0,2.8,-1.3,5.2,-2.9c2.3,-1.6,5,-2.8,6,-2.8c2.6,0,3.8,-2,3.2,-4.9c-0.4,-2,0,-3.2,2.2,-6.3l2.7,-3.8L265,434.8c-1.2,-1,-4.4,-3.5,-7.2,-5.7c-4.1,-3.1,-5.5,-4.8,-7.5,-9.3c-1.4,-3,-2.5,-5.7,-2.5,-6c0,-0.3,1.4,-1,3.1,-1.5c2.6,-0.9,3.2,-0.8,4.1,0.4c0.6,0.7,1,1.9,1,2.4c0,0.8,1.2,1.1,3.8,1.1c3.1,0,3.9,-0.3,4.2,-1.6c0.2,-0.9,0.9,-1.7,1.4,-1.7c1.1,0,4.8,3.6,4.3,4.2c-0.2,0.2,-0.5,1.6,-0.8,3.1l-0.6,2.8l5.5,-0.4l5.5,-0.5l9.8,9.2c8.5,8,9.6,9.3,9.2,11.2c-0.4,1.9,0.3,2.8,5.3,7c3.2,2.7,5.8,5.2,5.8,5.7c0,0.4,-3.2,3.2,-7.1,6.1c-5.8,4.3,-7.9,6.5,-11.3,11.8c-2.8,4.3,-4.8,8.7,-5.8,12.4c-0.8,3.1,-2.2,7.3,-3,9.3c-0.8,1.9,-1.5,4,-1.5,4.5c0,0.6,2,3.8,4.5,7.1c2.5,3.3,4.5,6.3,4.5,6.6c0,0.4,-6.7,5.9,-14.8,12.4c-13.2,10.4,-25.7,18.6,-28.5,18.7c-0.5,0,-7.8,-8.2,-16,-18.3Z',
    name: 'kv. Ovcha kupel'
  },
  {
    svgPathDescription: 'M445,547.1c0,-2,0.6,-4.3,1.2,-5.1c0.9,-1.2,1.2,-4.2,1.3,-11.9c0,-9.7,-0.1,-10.4,-2.1,-13.4c-1.5,-2.1,-3.6,-3.8,-6.6,-5.1c-2.5,-1.1,-4.5,-2.3,-4.5,-2.7c0,-1.9,11.7,-10.5,25.7,-18.8c8.5,-5.1,15.4,-9.3,15.4,-9.4c0,-0.1,-0.5,-4.6,-1.2,-10c-1.9,-15.5,-3.1,-13.4,10.5,-18.8c6.4,-2.6,14.2,-5.8,17.2,-7.2l5.5,-2.5l6.4,5.8c3.5,3.2,7.8,7.2,9.6,9.1l3.2,3.2L526,475.2L525.4,490l-2.6,0c-3.9,0,-4.4,0.9,-3.8,6.9c0.5,4.1,1.1,6,3,8.3c1.3,1.7,2.7,3.5,3.1,4.1c0.6,0.8,-1.3,3.3,-7.3,9.5l-8.2,8.5l-8.2,2.9c-4.6,1.7,-15.5,5.8,-24.3,9.3c-16.9,6.7,-29.2,11.1,-31,11.1c-0.7,0,-1.1,-1.1,-1.1,-3.5Z',
    name: 'Lozenets',
  },
  {
    svgPathDescription: 'M602.2,552.9c-6.6,-9.3,-11.4,-18.1,-14.3,-26.4c-3.6,-10.1,-6.5,-13.9,-12.8,-16.8c-1.9,-1,-3.5,-2,-3.5,-2.5c0,-0.4,1.2,-1.6,2.6,-2.6c1.5,-1,6.8,-6.1,11.9,-11.3l9.1,-9.5l8.9,8.2c10.1,9.1,39,38.2,39,39.2c0,0.3,-1,1,-2.1,1.4c-1.2,0.4,-8.3,5.3,-15.8,10.9c-17.3,12.7,-17.6,12.9,-19.2,12.9c-0.7,0,-2.4,-1.6,-3.8,-3.5Z',
    name: 'Iztok',
  },
  {
    svgPathDescription: 'M868.2,567.7c-1.6,-0.4,-6.8,-1.9,-11.6,-3.3l-8.8,-2.5l-0.3,-7.1c-0.2,-4.8,0.1,-8,0.9,-10.1c1.7,-4.4,0.2,-8.9,-3.6,-11.3c-2.1,-1.3,-2.9,-2.4,-2.9,-3.8c0,-1.2,-0.9,-3.2,-2.1,-4.6c-2.1,-2.5,-2.1,-2.5,-0.5,-4.1c1.3,-1.3,1.9,-1.4,3.4,-0.7c0.9,0.6,1.7,1.5,1.7,2c0,1.8,2.9,3.9,5,3.6c1.6,-0.1,2,0.3,2.3,1.9c0.1,1.1,0.6,2.5,1.1,3c0.5,0.6,0.6,2.3,0.3,4.1c-0.6,2.8,-0.4,3.2,4.6,8.2c2.8,2.8,5.8,5.2,6.6,5.2c1.7,0,1.9,2.1,0.6,6.5c-1,3.5,-0.1,4.3,6.8,6.3c5.4,1.5,6.3,2.6,4.7,5.7c-1.2,2.1,-3.2,2.4,-8.2,1Z',
    name: 'Abdovitsa'
  },
  {
    svgPathDescription: 'M164.1,571.2c-2,-1.3,1.3,-9.1,6.4,-15.6l4.6,-5.7l2.7,-13c1.5,-7.1,3.3,-15.4,4,-18.4L183,513l5.8,-3.3l5.7,-3.3l8.9,0.6l8.9,0.6l15.3,18.6c8.5,10.3,15.1,19.1,14.9,19.5c-0.8,1.2,-36.2,18.9,-37.8,18.9c-0.9,0,-1.3,-0.9,-1.3,-2.5c0,-1.3,-0.3,-2.5,-0.6,-2.8c-1.3,-0.8,-12.1,2.1,-13.8,3.7c-2.3,2.1,-18.8,8.9,-21.6,8.9c-1.2,0,-2.7,-0.3,-3.3,-0.7Z',
    name: 'Karpuzitsa'
  },
  {
    svgPathDescription: 'M390.4,571.4C384.1,567.1,375.7,562,371.8,560c-6.8,-3.4,-20,-13.7,-19.5,-15.2c0.5,-1.4,21.4,-29.4,21.9,-29.4c1.4,0,19.3,19.1,23.8,25.2c4.5,6.3,4.9,7.1,4.5,10.1c-0.4,3.3,1.5,9.8,3.6,12.3c1.8,2.1,1.4,10.3,-0.6,13.5c-0.9,1.6,-2.1,2.8,-2.7,2.8c-0.5,-0.1,-6.1,-3.6,-12.4,-7.9Z',
    name: 'Gotse Delchev'
  },
  {
    svgPathDescription: 'M625.2,574.2c-4,-3.7,-9.4,-8.4,-12,-10.5c-2.5,-2,-4.6,-4,-4.6,-4.5c0,-0.4,7.3,-6.1,16.2,-12.6L641,534.9l3,3.5c2.7,3,3.2,4.3,3.6,8.9c0.3,2.9,1.2,7.9,2.1,10.9l1.5,5.6l-4.1,4.8c-3.3,4.1,-12.9,12.3,-14.2,12.3c-0.2,0,-3.6,-3,-7.7,-6.7Z',
    name: 'Musagenitsa'
  },
  {
    svgPathDescription: 'M274.6,574.3c-7.7,-3.6,-14.3,-6.9,-14.9,-7.3c-0.5,-0.4,-2,-3.5,-3.3,-6.7c-1.2,-3.3,-3.7,-7.6,-5.4,-9.6c-1.7,-2,-2.9,-4,-2.7,-4.4c0.3,-0.3,3.1,-2,6.3,-3.6c3.3,-1.6,9,-5.3,12.7,-8.2l6.8,-5.3l13.1,7.6c7.3,4.2,13.2,8.1,13.2,8.6c0,0.6,-0.8,3.2,-1.8,5.8c-0.9,2.6,-3.1,10.3,-4.8,17c-2,8.4,-3.4,12.4,-4.2,12.5c-0.6,0.1,-7.4,-2.8,-15,-6.4Z',
    name: 'Pavlovo'
  },
  {
    svgPathDescription: 'M443.8,568.4c0.4,-8.1,0.9,-13.6,1.5,-14.2c1,-1,8.4,-3.6,10.1,-3.6c1.5,0,1.4,1.7,-0.6,14.5L453,576l-3.9,2.9c-2.1,1.6,-4.3,2.9,-4.9,2.9c-0.7,0,-0.8,-3.1,-0.4,-13.4Z',
    name: 'Hladilnika'
  },
  {
    svgPathDescription: 'M541.2,581.7c0,-0.5,2.6,-5.2,5.9,-10.4l5.9,-9.5l0,-8.1l0.1,-8l7,-2.2c3.8,-1.2,10.5,-2.4,14.8,-2.8c4.3,-0.4,9.2,-1.1,11,-1.5c1.8,-0.4,3.6,-0.6,4,-0.3c0.3,0.2,2.7,3.6,5.2,7.5c2.5,3.9,5.3,7.9,6.2,8.9c0.9,1,1.5,2.2,1.4,2.7c-0.2,0.4,-6.2,3.6,-13.4,7c-7.1,3.4,-14.5,7.2,-16.4,8.5c-2.3,1.6,-7.5,3.4,-16,5.7c-13.2,3.6,-15.7,4,-15.7,2.5Z',
    name: 'Dianabad'
  },
  {
    svgPathDescription: 'M676.6,559.6c-25.4,-26,-27.3,-28.1,-27.1,-30.5c0.3,-2.1,2.8,-5,13.2,-15.4l12.9,-12.8l9.2,2.5l9.3,2.5l-0.2,3.4c-0.2,2.9,0.7,4.9,5.5,13.1c5.4,9.5,20.6,32.8,23.2,35.8c1.3,1.5,1.4,1.4,2.8,-0.4l1.5,-1.9l1.6,1.9c2,2.4,1.4,4,-4.2,11.8c-3.7,5.1,-17.7,17.9,-19.5,17.9c-0.4,0,-13.1,-12.6,-28.2,-27.9Z',
    name: 'Poligona'
  },
  {
    svgPathDescription: 'M519.8,588.2c0,-0.5,0.6,-2.9,1.4,-5.3c1.3,-4.2,1.5,-4.4,4,-4.4c3.4,0,3.9,0.7,3.2,5.3c-0.5,3.4,-0.8,3.7,-3.5,4.5c-3.9,1.1,-5.1,1.1,-5.1,-0.1Z',
    name: 'Sofia Zoo'
  },
  {
    svgPathDescription: 'M865.1,605.7c-2.3,-2.1,-4.3,-4.1,-4.3,-4.5c0,-0.3,1,-1,2.1,-1.4c1.8,-0.7,2,-1.3,2,-4.4c0,-3.4,-0.3,-3.9,-4.4,-7.5c-2.7,-2.2,-4.9,-4.9,-5.4,-6.5c-0.6,-1.9,-2.1,-3.5,-5,-5.2L846,573.6l0,-4.5c0,-5.1,0.1,-5.1,5.8,-3.4c3.9,1.2,5,3.2,1.8,3.6c-3.3,0.5,-2.1,3.1,2.5,5.5c2.4,1.2,5.7,2.9,7.5,3.8c2.9,1.5,3.1,1.9,2.7,4.4c-0.3,2.3,-0.1,3,1.7,4.1c1.2,0.8,2.8,1.2,4,0.9c1.6,-0.4,2,-0.2,2,1.1c0,1,-0.6,3.1,-1.3,4.7c-1.1,2.8,-1.1,3.1,0.3,4.7c0.9,0.9,1.6,2.3,1.6,2.9c0.1,0.7,0.3,2.1,0.6,3c0.7,2.1,-2,5.2,-4.3,5.2c-0.8,0,-3.4,-1.8,-5.8,-3.9Z',
    name: 'Dimitar Milenkov'
  },
  {
    svgPathDescription: 'M776.1,610C770.3,603.9,760,595.8,756.4,594.4c-2,-0.8,-3.9,-1.7,-4.3,-2c-0.4,-0.4,0.1,-2.2,1.1,-4c1.7,-2.9,1.7,-3.5,0.7,-4.8c-0.9,-1.2,-25,-29.3,-27.2,-31.8c-0.5,-0.5,-1,-0.3,-1.5,0.7c-0.5,0.8,-1.3,1.4,-1.8,1.4c-1.2,0,-17,-23.6,-23.3,-34.6c-3.1,-5.5,-4.1,-7.9,-3.8,-9.7c0.2,-1.3,0.7,-2.4,1.1,-2.4c0.4,0,11.1,2.6,23.8,5.7l23,5.8l19.5,13.6c10.7,7.4,19.9,14,20.5,14.5c1.3,1.3,-3.9,14.5,-11.7,29.4l-5.4,10.4l10.7,5.4c5.9,2.9,10.7,5.7,10.7,6.2c0,1.6,-7.6,14.7,-8.5,14.7c-0.6,0,-2.3,-1.3,-3.9,-2.9Z',
    name: 'zh.k. Druzhba 1'
  },
  {
    svgPathDescription: 'M65.4,627c0,-2,7.1,-6.7,26.3,-17.5c7.1,-4,13.1,-7.9,13.6,-8.9c3.2,-6.1,14,-11.6,36.9,-18.8c10.8,-3.4,16.1,-5.4,17,-6.6c1.3,-1.6,2,-1.7,5.9,-1.2c3.5,0.4,5.5,0.2,8.9,-1.1c8.3,-3.1,14.2,-6,16.3,-7.9c1.4,-1.3,3.5,-2.2,6.4,-2.6l4.3,-0.5l0,2.5c0,1.4,0.2,3,0.4,3.7c0.4,0.9,-0.5,1.8,-2.7,2.9c-3.2,1.6,-3.2,1.7,-3.7,7.5c-0.3,3.3,-0.7,6.1,-0.9,6.3c-0.2,0.2,-2.2,-0.2,-4.4,-0.8c-2.2,-0.7,-7.4,-1.5,-11.6,-1.7L170.4,582l-4.7,3.7c-2.5,2,-4.9,4,-5.4,4.4c-0.4,0.4,0.4,1.9,2.2,3.8c3.9,4,6.6,9.4,5.9,12c-0.5,2.3,-9,10.6,-10.1,9.9c-0.4,-0.2,-0.9,-1.7,-1.2,-3.2c-0.4,-2.1,-1.2,-3.1,-2.9,-3.8c-2,-0.8,-2.5,-1.5,-2.7,-3.9c-0.2,-2.3,-0.8,-3.3,-2.3,-4.1c-3,-1.4,-10.2,-1.3,-11.5,0.3c-0.8,0.9,-1.9,1.1,-4.2,0.7c-3,-0.5,-3.1,-0.4,-6.8,4.8c-4.8,6.7,-5.4,7.1,-10.1,7.8c-3.7,0.5,-4,0.7,-4.2,3.2c-0.3,3.2,-1.3,3.3,-6.2,0.9l-3.6,-1.7l-4.9,1.7c-2.6,0.9,-8.9,3.3,-14,5.4c-7.1,2.9,-10.2,3.8,-13.8,3.8c-2.5,0,-4.5,-0.3,-4.5,-0.7Z',
    name: 'Knyazhevo'
  },
  {
    svgPathDescription: 'M669.8,630.4c-6.7,-2.5,-19.9,-5.5,-26.3,-5.9c-6,-0.5,-6.6,-0.7,-6.3,-2.2c0.1,-0.9,0.6,-4.9,0.9,-8.9c0.6,-6.8,0.8,-7.4,3.2,-9.3c2.1,-1.6,2.4,-2.2,1.8,-3.5c-1.2,-2.2,-1.1,-3,0.8,-3.5c1.2,-0.3,1.6,-1.1,1.6,-3.3c0,-2.6,-0.4,-3.2,-4.7,-6c-2.6,-1.8,-4.7,-3.8,-4.7,-4.5c0,-0.7,2.2,-3.1,4.9,-5.4c2.7,-2.2,6.7,-6.1,8.9,-8.7l3.9,-4.6l-1.6,-6c-0.9,-3.3,-1.8,-8.5,-2.1,-11.6c-0.4,-4.3,-1.1,-6.4,-2.6,-8.4c-1.1,-1.4,-2,-3,-2,-3.5c0,-3.3,5.4,1.5,30,26.8l26.8,27.6l-4.9,4.6c-4.5,4,-5.7,6,-13.2,21.1c-8.9,17.9,-8.5,17.4,-14.4,15.2Z',
    name: 'Mladost 1'
  },
  {
    svgPathDescription: 'M359.6,617.3c-15.2,-8.4,-36.1,-19.4,-46.5,-24.6C302.7,587.5,293.9,583,293.5,582.6c-0.4,-0.4,0.5,-5.3,2,-11.2c3.3,-13.2,14,-45.4,15.1,-45.4c0.5,0,2.1,1.3,3.7,2.8c2,2,8.1,5.2,19,10c14.7,6.6,16.5,7.7,22.6,13.1c3.6,3.2,8.9,7.1,11.9,8.5c2.9,1.5,11.9,7,19.9,12.2c10.7,7.1,14.4,9.9,14.1,10.8c-4.1,9.8,-4.6,12.1,-5.3,24.6c-0.5,8.4,-1.2,13.8,-2,15.9c-2,4.7,-4.8,8.7,-6.2,8.6c-0.7,0,-13.6,-6.8,-28.7,-15.2Z',
    name: 'kv. Manastirski livadi'
  },
  {
    svgPathDescription: 'M617.8,636.8c-6.1,-2,-9.9,-3.7,-11.4,-5.2l-2.1,-2.1l3.2,-4.8c1.8,-2.7,4.3,-6.8,5.5,-9.3c2.1,-4.1,2.3,-4.9,1.7,-10.6c-0.3,-3.4,-1,-9.2,-1.7,-12.9c-1.4,-7.5,-1.3,-24.2,0.1,-24.6c0.5,-0.2,6.2,4.6,12.8,10.6c6.5,6,13.2,11.7,14.9,12.8c4.1,2.5,4.1,4.2,-0.2,4.2c-3.7,0,-4,0.5,-1.6,3.5c2,2.6,2,3.4,-0.1,4.8c-2,1.3,-3.2,5.9,-3.2,12c0,2.9,-0.5,5,-1.6,6.5c-0.9,1.3,-2,4.8,-2.5,8c-1.5,9.6,-1.7,10.3,-3.2,10.2c-0.7,-0.1,-5.5,-1.5,-10.6,-3.1Z',
    name: 'kv. Darvenitsa'
  },
  {
    svgPathDescription: 'M692.1,637.5C685,635.4,678.9,633.3,678.6,633c-0.6,-0.6,13.6,-29.6,15.1,-30.9c1.4,-1.1,7.4,2,8.6,4.5c0.5,1,3.5,5.2,6.7,9.4c5.4,7,5.7,7.8,5.3,10.6c-0.3,1.7,-1.7,5.3,-3,7.9c-1.4,2.7,-2.5,5.4,-2.5,6c0,1.8,-2.9,1.2,-16.7,-3Z',
    name: 'Mladost 1A'
  },
  {
    svgPathDescription: 'M742.2,653c-5.4,-2.2,-13.9,-5.1,-18.9,-6.5c-13.5,-3.7,-13,-3.4,-11.7,-7.1c0.6,-1.7,1.9,-4.4,2.8,-6c0.9,-1.6,1.9,-4.6,2.2,-6.7c0.6,-3.9,0.6,-3.9,-5.7,-11.8c-3.4,-4.4,-6.2,-8.6,-6.2,-9.4c0,-0.8,-1.7,-2.3,-4.1,-3.6c-2.3,-1.2,-4.1,-2.6,-4.1,-3.2c0,-0.5,2.3,-2.8,5.1,-5.1c16.9,-13.7,21.7,-18.3,25.7,-24.4c2.3,-3.4,4.5,-6.2,4.9,-6.2c1.1,-0.1,19.3,20.8,19.3,22.1c0,0.6,-1.5,3.6,-3.3,6.8c-3.4,5.8,-3.4,5.9,-3.1,12.8c0.3,6.4,0.6,7.5,3.3,11.9c1.7,2.7,6,7.9,9.7,11.4l6.6,6.6l0,10l0,10l-3.1,0.9c-7.2,2.1,-9.2,1.8,-19.4,-2.5Z',
    name: '7th 11th kilometer'
  },
  {
    svgPathDescription: 'M277.4,657.4c-6.7,-1,-9.6,-1.1,-13,-0.4c-3.8,0.8,-4.7,0.7,-6.2,-0.4c-1.5,-1.2,-1.8,-2.2,-1.9,-8c-0.1,-8.2,-3.2,-16.2,-7,-18.1c-2.2,-1.2,-2.3,-1.2,-4,2.1c-2.2,4.3,-4.8,4.3,-6.2,0c-0.7,-2,-1.6,-3,-3.2,-3.4c-3.8,-1.1,-4.9,-4.6,-2.3,-7.4c1.3,-1.5,1.3,-1.7,0,-4.4c-1.6,-3.2,-1.7,-5.2,-0.7,-6.8c0.6,-0.9,2.4,0.1,7.8,4.5c3.8,3.1,8,6.2,9.2,6.8c2,1.1,2.3,1.1,3.8,-0.7l1.5,-2l7.6,3.4c11.7,5.1,16.7,6.1,27.2,5.7l8.8,-0.5l1.2,2.3c0.6,1.3,2.3,2.7,3.7,3.2c1.3,0.4,2.5,1.2,2.5,1.6c0,2.4,-1.8,4.9,-4,5.7c-2.2,0.8,-2.5,1.4,-3.9,7.9c-1.4,6.7,-1.6,7.1,-3.8,7.6c-1.3,0.2,-2.3,1,-2.3,1.6c0,1.4,-3.2,1.3,-14.8,-0.3Z',
    name: 'Boyana'
  },
  {
    svgPathDescription: 'M444.1,764c-2.7,-1.1,-3.2,-1.7,-4,-5.4c-1.2,-5.5,-3.9,-10.2,-6.5,-11.2c-1.2,-0.5,-3.9,-1.1,-6,-1.4c-3.5,-0.5,-3.9,-0.8,-5.1,-3.6c-0.7,-1.8,-3.8,-5.5,-7.7,-9.1c-5.9,-5.5,-6.6,-6.4,-7.1,-9.8c-0.5,-3.4,-0.3,-4,2.4,-7.1c1.6,-1.9,3.7,-5.2,4.7,-7.4c0.9,-2.1,2.6,-5.3,3.7,-7.1c1.2,-1.8,2.9,-5.6,3.8,-8.6c1.7,-5,1.8,-6.1,1,-13.8c-0.5,-4.5,-1.2,-8.6,-1.5,-8.9c-0.3,-0.4,-2.1,0.7,-3.8,2.4l-3.1,3.1l-5.3,-2.5c-2.9,-1.4,-6.8,-2.9,-8.7,-3.3c-2,-0.5,-3.5,-1.3,-3.5,-1.8c0,-0.6,2.6,-5.1,5.9,-10.2c4.4,-6.8,6.4,-9.2,7.4,-9c1.4,0.3,87.3,48.7,91.2,51.4l2.2,1.6l-13.5,19.2c-13.8,19.6,-15.2,21.9,-15.2,25.4c0,1.5,-1.2,2.9,-4.3,5.2c-5.8,4.2,-21.4,13.2,-22.8,13.2c-0.7,0,-2.6,-0.6,-4.2,-1.3Z',
    name: 'kv. Dragalevtsi'
  },
  {
    svgPathDescription: 'M774.5,764.5c-3.3,-0.4,-9.1,-1.6,-12.7,-2.8c-3.6,-1.2,-9.5,-2.7,-13.1,-3.3c-3.5,-0.7,-6.6,-1.4,-6.8,-1.6c-0.2,-0.2,0.4,-3.8,1.4,-7.8l1.9,-7.5l-2.3,-8.7c-2.3,-8.5,-2.5,-8.8,-8.8,-16.1c-3.9,-4.4,-6.3,-7.9,-6.1,-8.7c0.2,-0.7,0.6,-2.5,0.9,-3.9c0.7,-3.2,1.9,-3.4,11.5,-1.1c7.2,1.7,8.2,1.5,9,-2.4c0.4,-2.4,0.6,-2.5,5,-2.5c2.5,0,6.2,0.5,8.2,1c3.4,0.9,3.9,0.9,6.1,-0.8c1.4,-1,2.7,-1.8,2.9,-1.8c0.3,0,2,1.7,3.9,3.7c1.9,2.1,3.8,3.6,4.3,3.5c2.4,-0.6,12,-6.9,12,-7.8c0,-3.7,-10.5,-20.3,-19.2,-30.1c-7.1,-8.1,-3.9,-7.2,14.3,4.1c9.3,5.7,18.9,12.1,21.3,14.3c5.5,4.6,7.5,5.7,10.3,5.7c2.6,0,2.6,1.4,-0.1,4.2c-3,3.1,-3.6,6.8,-4.3,26.6l-0.7,17.2l-3.5,6.9c-3,5.9,-3.5,7.7,-3.4,11.3l0.2,4.3l-6.1,0c-5.6,0,-6.2,0.2,-8.4,2.5c-2.6,2.7,-4.6,2.9,-17.7,1.6Z',
    name: 'kv. Gorublyane'
  },
  {
    svgPathDescription: 'M666.5,753.3c-13.6,-0.6,-25.1,-1.3,-25.7,-1.5c-0.6,-0.2,-1,-1.6,-1,-3.2c0,-4.3,11.5,-19.9,15.6,-21.3l2.9,-0.9l0,-11l0,-10.9l-3.1,-2.7c-1.7,-1.4,-3.1,-2.8,-3.1,-3.1c0,-0.3,5.4,-0.6,12,-0.6l12,0l-0.3,-4.7l-0.3,-4.7l6.5,-0.2c5.3,-0.2,7.8,0.2,14.9,2.4c5.2,1.5,8.6,3,8.6,3.6c0,3.2,-2.8,6.8,-6.3,8.1c-3,1.2,-3.5,1.7,-3.5,3.7c0,4.3,-0.7,4.9,-5.1,4.9l-4,0l0,4c0,3.6,0.3,4.2,4.2,8c3.3,3.2,5.3,6.4,9.5,15c3.2,6.8,5.2,12,5.2,13.7l0,2.8l-7.2,-0.1c-3.9,-0.1,-18.3,-0.7,-31.8,-1.3Z',
    name: 'Mladost 4'
  },
  {
    svgPathDescription: 'M550,724c-16.7,-9.1,-30.6,-17,-31.1,-17.4c-0.6,-0.6,-0.2,-8.1,1,-23.2c2.1,-24.6,1.8,-23.6,7.8,-24c2.8,-0.2,3.1,0,3.8,2.5c0.7,2.6,0.8,2.6,5.4,2.7c11.4,0.2,13.1,0.7,25.1,8.4c6.2,3.9,11.8,7.1,12.5,7.1c1.9,0,5.5,-8.8,6.1,-14.7L581,660l3.5,-0.7c2,-0.4,3.7,-0.5,4,-0.3c0.2,0.2,2.7,12.1,5.4,26.4c4.6,23.4,5,26.4,4.1,29.6c-1,3.7,-15.6,25.8,-17,25.7c-0.4,0,-14.4,-7.5,-31,-16.7Z',
    name: 'Malinova Dolina'
  },
  {
    svgPathDescription: 'M509.5,660.1c0.7,0,1.3,0.1,1.8,0.3c0.6,0.2,1.1,0.7,1.3,1.2c0.3,0.6,0.4,1,0.5,1.6c0.6,2.8,0.8,5.6,0.6,7.9c-0.1,2.4,-0.6,4.4,-1.6,5.8c-0.5,0.7,-1,1.3,-1.5,1.7c-0.3,0.3,-0.5,0.5,-0.8,0.7c-0.3,0.1,-0.6,0.3,-1.1,0.3c-0.5,0,-0.9,-0.2,-1.3,-0.3c-0.4,-0.2,-0.8,-0.4,-1.4,-0.7c-1,-0.5,-2.1,-1.1,-3.2,-1.8c-1.2,-0.7,-2.2,-1.4,-3,-2.1c-0.4,-0.3,-0.8,-0.6,-1.1,-0.9c-0.1,-0.1,-0.3,-0.3,-0.4,-0.5c-0.1,-0.2,-0.3,-0.4,-0.3,-0.9c0,-0.4,0.1,-0.6,0.2,-1c0.1,-0.4,0.2,-0.9,0.3,-1.4c0.3,-1.1,0.7,-2.4,1.1,-3.8l1.4,-4.9l0.3,-0.9l0.9,0l4.6,-0.2c1.1,-0.1,1.9,-0.1,2.7,-0.1Zm40,-76.6l-6.8,1.9c-3.8,1.1,-11.3,3.2,-16.6,4.8c-13.7,4.2,-16.9,4.5,-24.1,1.9c-4.9,-1.8,-7.2,-2.1,-14.8,-2.1l-9,0l-3.5,14.4c-2.9,12.1,-3.5,16.3,-3.9,25.6c-0.4,10.6,-0.6,11.6,-3.7,19.5c-3.4,8.6,-7.5,23.2,-6.8,24c0.2,0.3,12.3,7.4,26.9,15.8c14.5,8.4,26.7,15.3,27,15.3c1.3,0.2,1.7,-2.5,3.3,-23.1c1,-11.8,2,-21.9,2.4,-22.4c0.4,-0.5,2.8,-1.3,5.2,-1.9c2.5,-0.5,4.6,-1.3,4.6,-1.8c0,-0.5,-0.6,-3.1,-1.3,-5.8l-1.3,-4.9l2.4,-4.2c2.1,-3.8,2.4,-5.3,2.9,-13.6c0.4,-8.8,0.7,-10,3.7,-16.5c2.9,-6,4.1,-7.5,9.1,-11.7c5.3,-4.5,5.8,-5.1,5.8,-8c0,-1.7,-0.3,-4,-0.7,-5.2Z',
    name: 'kv. Vitosha'
  },
  {
    svgPathDescription: 'M506.9,662.6l-3.8,0.2l-1.2,4.1c-0.4,1.4,-0.8,2.7,-1,3.7c-0.2,0.5,-0.2,1,-0.3,1.3c0,0.2,0,0.2,-0.1,0.3c0.2,0.1,0.4,0.3,0.8,0.6c0.7,0.5,1.7,1.2,2.8,1.9c1,0.7,2.1,1.3,3,1.8c0.5,0.2,0.9,0.4,1.2,0.5c0.2,0.1,0.3,0.1,0.3,0.1c0.1,-0.1,0.2,-0.1,0.3,-0.3c0.4,-0.3,0.9,-0.8,1.2,-1.3c0.6,-0.7,1,-2.4,1.2,-4.5c0.1,-2.2,-0.1,-4.7,-0.6,-7.3c-0.1,-0.6,-0.2,-0.9,-0.3,-1c-0.1,-0.1,-1.3,-0.2,-3.5,-0.1Z',
    name: 'Gradina'
  },
  {
    svgPathDescription: 'M655.1,695.1l-3.2,-0.4l0.6,-3.8c0.7,-4.5,23.2,-55,24.6,-55.1c0.5,0,7.5,2,15.5,4.3l14.7,4.4l-0.5,3c-0.3,2,0.1,5.5,1.2,9.6c2,7.9,2.2,18.7,0.4,22.2c-1.3,2.4,-1.2,2.4,2,4.1c3.7,1.9,4.3,4,1.9,7.2l-1.5,1.9L700,689.2c-9.6,-3,-11.8,-3.4,-19.2,-3.4l-8.4,0l0.6,5l0.6,4.9l-7.7,-0.2c-4.2,0,-9.1,-0.2,-10.8,-0.4Z',
    name: 'Mladost 3'
  },
  {
    svgPathDescription: 'M639.9,686.4c-1,-1,-1.8,-2.9,-1.8,-4c0,-1.9,-0.3,-2.2,-3,-2.4c-3.1,-0.3,-3.1,-0.3,-3,-3.7c0.1,-2.2,1,-4.7,2.4,-6.7c1.9,-2.9,2.2,-4.3,2.4,-10.6c0.2,-6.3,0.6,-8,3.3,-13.7c2.3,-5,3.1,-8,3.5,-12.5c0.5,-6.9,0.2,-6.7,9,-4.7c9.4,2.1,20.1,5.6,20.5,6.6c0.2,0.6,-4.7,12.5,-11,26.5C651,686.3,650.8,686.7,648,687.5c-4.4,1.2,-6.1,1,-8.1,-1.1Z',
    name: 'Mladost 2'
  },
  {
    svgPathDescription: 'M822.7,687.7l-6.6,-0.3l-5.6,-4.7c-3.2,-2.5,-14.1,-9.8,-24.3,-16.1L767.6,655.1l-0.3,-11.2l-0.2,-11.3l-6.2,-5.5c-3.5,-3.1,-7.7,-7.6,-9.3,-10.1c-2.8,-4,-3.2,-5.2,-3.7,-11.6c-0.5,-5.3,-0.3,-7.5,0.5,-8.8c1.4,-2.2,2.1,-2.2,7.4,0.2c5.4,2.4,9.2,5.7,29.8,25.5c19.4,18.6,36.3,34,37.3,34c0.4,0,1.1,-0.9,1.6,-2c0.8,-1.8,1.5,-2.1,5,-2.1c4.3,0,15.7,2.2,16.7,3.1c0.3,0.4,-1.7,7.9,-4.5,16.8l-5,16.2L833,688.1c-2.1,-0.1,-6.7,-0.3,-10.3,-0.4Z',
    name: 'Druzhba 2'
  },
  {
    svgPathDescription: 'M561.9,670c-11.4,-7.2,-13.6,-7.9,-24.1,-7.9c-1.7,0,-3.3,-0.4,-3.6,-0.9c-0.3,-0.4,-1.4,-4.3,-2.4,-8.5l-1.9,-7.8l2.3,-3.9c1.9,-3.4,2.3,-5.2,2.8,-13.7c0.6,-8.8,0.9,-10.3,3.6,-16c2.5,-5.4,3.9,-7.1,8.7,-11.2c3.2,-2.6,6,-5.1,6.3,-5.4c0.2,-0.4,0.1,-3.1,-0.3,-6.1c-0.4,-3,-0.6,-5.5,-0.4,-5.7c0.2,-0.2,4.1,-1.4,8.5,-2.5c4.8,-1.3,9.6,-3.1,11.6,-4.5c3.4,-2.4,30.7,-15.4,32.1,-15.4c0.4,0,1.5,0.6,2.5,1.5c1.7,1.4,1.8,2.3,1.8,12.6c0,6.1,0.5,14,1.2,17.5c2.4,13.4,2.5,16.9,0.2,21.9c-1.2,2.5,-3.5,6.7,-5.3,9.3c-1.8,2.6,-3.5,5.2,-3.8,5.7c-0.3,0.6,0.7,2.2,2.4,3.8l3.1,2.9l-3.7,3.9c-3.3,3.6,-3.8,4.7,-4.6,9.8c-0.8,4.8,-1.4,6.1,-3.6,8.1c-2.5,2.2,-2.7,2.2,-3.7,0.9c-0.6,-0.8,-1.2,-1.8,-1.3,-2.3c-0.2,-0.4,-2.7,-0.1,-6,0.8l-5.7,1.5l-0.5,6.1c-0.4,6.1,-2.7,12.3,-4.4,12.3c-0.5,0,-5.8,-3.1,-11.8,-6.8Z',
    name: 'zh.k. Studentski grad'
  },
  {
    svgPathDescription: 'M424.1,653.4C406.4,643.3,392,634.8,392,634.4c0,-0.3,1.4,-3.4,3.1,-6.7c3.1,-6.1,3.1,-6.3,3.8,-19.7c0.8,-14.9,1.4,-17.1,7.4,-27.6c1.9,-3.3,3.4,-6.7,3.4,-7.6c0,-2.9,2.3,-1.9,4.2,1.8c3.1,6.2,5.7,8.9,10.9,11.3c5.2,2.5,9.5,3,13.1,1.6c1.8,-0.6,2.4,-0.4,3.6,1.2c1.4,1.9,1.7,1.9,8,1.2c3.7,-0.4,10.8,-0.7,15.9,-0.7c7.3,-0.1,9.4,0.2,9.7,1.1c0.3,0.7,-1.1,7.4,-2.9,15c-2.8,11.3,-3.5,15.7,-3.9,24.8c-0.5,10.1,-0.8,11.7,-3.4,18.5c-1.6,4,-3.8,10.8,-5,15c-1.2,4.3,-2.5,7.9,-3,8c-0.4,0.1,-15.2,-8.1,-32.8,-18.2Z',
    name: 'Krastova vada'
  }
];

export const sofiaRegions: Region[] = pathDescriptions.map((desc, index) => ({
  svgPathDescription: desc, name: 'Region ' + index
})).concat(sofiaRegions_real);
