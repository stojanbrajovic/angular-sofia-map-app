import { Component, OnInit, Input } from '@angular/core';
import { Region } from '../region-model/region';

@Component({
  selector: 'app-regions-menu',
  templateUrl: './regions-menu.component.html',
  styleUrls: ['./regions-menu.component.css']
})
export class RegionsMenuComponent implements OnInit {

  @Input() regions: Region[];
  @Input() selectedRegions: Region[];
  @Input() onRegionClick: (region: Region) => void;

  constructor() { }

  ngOnInit() {
  }

}
