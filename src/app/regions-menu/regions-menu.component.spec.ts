import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionsMenuComponent } from './regions-menu.component';

describe('RegionsMenuComponent', () => {
  let component: RegionsMenuComponent;
  let fixture: ComponentFixture<RegionsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
