import { Component, OnInit } from '@angular/core';
import { sofiaRegions } from '../region-model/sofia-regions';
import { Region } from '../region-model/region';

@Component({
  selector: 'app-sofia-map',
  templateUrl: './sofia-map.component.html',
  styleUrls: ['./sofia-map.component.css']
})
export class SofiaMapComponent implements OnInit {

  regions = sofiaRegions;
  selectedRegions: Region[] = [];

  constructor() { }

  ngOnInit() { }

  onRegionClick = (region: Region) => {
    const indexInSelectedArray = this.selectedRegions.indexOf(region);
    if (indexInSelectedArray === -1) {
      this.selectedRegions.push(region);
    } else {
      this.selectedRegions.splice(indexInSelectedArray, 1);
    }
  }

}
