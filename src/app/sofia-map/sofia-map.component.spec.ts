import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SofiaMapComponent } from './sofia-map.component';

describe('SofiaMapComponent', () => {
  let component: SofiaMapComponent;
  let fixture: ComponentFixture<SofiaMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SofiaMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SofiaMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
