import { Component, OnInit, Input } from '@angular/core';
import { Region } from '../region-model/region';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  @Input() regions: Region[];
  @Input() selectedRegions: Region[] = [];
  @Input() height: number;
  @Input() width: number;
  @Input() onRegionClick = (region: Region) => {};

  constructor() { }

  ngOnInit() { }

}
